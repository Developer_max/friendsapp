<?php
	
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");


?>

<?php

require_once ('../../../../../friendsappinc/connection.php');
	
	$insertquery = $conn->prepare("select * from posts");

// check whether the above query runs
	$insertquery->execute();

    $results = $insertquery->get_result();

	if (mysqli_num_rows($results)>0){

		$array = array();

		// if execute then send all associate data in json format

		while ($rows = mysqli_fetch_assoc($results)) {
					
					$array[] = $rows;

				}


				$array['httpcode'] = 200;

				echo json_encode($array);



	}
	else{

		// else print the no content found


	$message = array('httpcode' => 204,'message'=>"no content");

		echo json_encode($message);

	}
	



?>