<?php

require_once ('../friendsappinc/connection.php');


$query = "select * from photos";
$result = mysqli_query($conn,$query);
$photocount = mysqli_num_rows($result);

$query = "select * from video";
$result = mysqli_query($conn,$query);
$videocount = mysqli_num_rows($result);

$query = "select * from story";
$result = mysqli_query($conn,$query);
$storycount = mysqli_num_rows($result);


?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    lanka hub development
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
    name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css"
    href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="assets/demo/demo.css" rel="stylesheet" />
</head>

<body class="dark-edition">
  <div class="wrapper ">
    <div class="sidebar" data-color="azure" data-background-color="black"
      data-image="">
      <div class="logo"><a href="#" class="simple-text logo-normal">
          Lanka Hub
        </a></div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item active  ">
            <a class="nav-link" href="in">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="./examples/tables.php">
              <i class="material-icons">content_paste</i>
              <p>Table List</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top " id="navigation-example">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="javascript:void(0)">Dashboard</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
            aria-expanded="false" aria-label="Toggle navigation" data-target="#navigation-example">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">

            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link" href="javscript:void(0)" id="navbarDropdownMenuLink" data-toggle="dropdown"
                  aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>

                  <p class="d-lg-none d-md-block">
                    Some Actions
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="javascript:void(0)">Sign Out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">

          <div class="row">
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">play_circle_filled</i>
                  </div>
                  <p class="card-category"><?php echo $videocount ?></p>
                  <h3 class="card-title">Video

                  </h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    Video section
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">camera_alt</i>
                  </div>
                  <p class="card-category"><?php echo $photocount ?></p>
                  <h3 class="card-title">Photos</h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    Models photo section
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-danger card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">picture_as_pdf</i>
                  </div>
                  <p class="card-category"><?php echo $storycount ?></p>
                  <h3 class="card-title">Stories</h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    Stories
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">

            <div class="col-lg-9 col-md-12">
              <div class="card">
                <div class="card-header card-header-tabs card-header-info">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <span class="nav-tabs-title">Category:</span>
                      <ul class="nav nav-tabs" data-tabs="tabs">
                        <li class="nav-item">
                          <a class="nav-link active" href="#video" data-toggle="tab">
                            <i class="material-icons">play_circle_filled</i> Video list
                            <div class="ripple-container"></div>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#Photo" data-toggle="tab">
                            <i class="material-icons">camera_alt</i> Photo list
                            <div class="ripple-container"></div>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#Stories" data-toggle="tab">
                            <i class="material-icons">picture_as_pdf</i> Stories list
                            <div class="ripple-container"></div>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="card-body">
                  <div class="tab-content">
                    <div class="tab-pane active" id="video">
                      <!-- Form content -->
                      <div class="card">
                        <div class="card-body">
                          <form method="POST" action="insert.php">
                            <br>
                            <div class="row">
                              <div class="col-6">
                                <input type="text" required="text" name="name" class="form-control" placeholder="Video name">
                                <br>
                                <input type="text" required="text" name="caption" class="form-control" placeholder="video caption">
                              </div>

                              <div class="col-6">
                                <input type="text" required="text" name="posterlink" class="form-control" placeholder="video poster link">
                                <br>
                                <input type="text" required="text" name="url" class="form-control" placeholder="video url">
                                
                                <input type="hidden" name="type" value="video">

                              </div>

                            </div>
                            <br>
                            <hr>
                            <div class="row">

                              <div class="col">
                                <textarea class="form-control" name="description" rows="5" spellcheck="true"
                                  placeholder=" Video description"></textarea>
                              </div>
                            </div>
                            <br>
                            <div class="row">
                               <input type="submit" name="submit" class="btn btn-success">
                            </div>

                          </form>
                        </div>
                      </div>

                    </div>
                    <div class="tab-pane" id="Photo">

                      <div class="card ">
                        <div class="card-body">
                          <form method="POST" action="insert.php">
                            <div class="row">
                              <div class="col">
                                <input type="text" name="name" required="text" class="form-control" placeholder="photo collection name">
                              </div>
                              <div class="col">
                                <input type="text" name="caption" required="text" class="form-control" placeholder="photo collection caption">
                              </div>
                              <div class="col">
                                <input type="text" name="thumbnail" required="text" class="form-control" placeholder="collection thumbnail url">
                                <input type="hidden" name="type" value="images">
                              </div>
                            </div>

                            <br>
                            <div class="row">
                              <div class="col">
                                <div class="row" id="imagelinkcont">

                                  <div class="col-6">
                                    <label for="add image link"></label>
                                    <button type="button" rel="tooltip" title="Add new image" id="addimagelink"
                                      class="btn  btn-white btn-link btn-sm">
                                      <i class="material-icons">add_circle</i> Add image link
                                    </button>
                                    <br>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <hr>
                            <div class="row">

                              <div class="col">
                                <textarea class="form-control" rows="5" spellcheck="true"
                                  placeholder=" collection Description"></textarea>
                              </div>
                            </div>
                            <br>
                            <div class="row">
                             <input type="submit" name="submit" class="btn btn-success">
                            </div>

                          </form>
                        </div>
                      </div>

                    </div>
                    <div class="tab-pane" id="Stories">


                      <div class="card">
                        <div class="card-body">
                          <form method="POST" action="insert.php">
                            <div class="row">
                              <div class="col">
                                <input type="text" class="form-control" name="name" placeholder="Story name">
                              </div>
                              <div class="col">
                                <input type="text" class="form-control" name="caption" placeholder="Story caption">
                              </div>
                              <div class="col">
                                <input type="text" class="form-control" name="thumblink" placeholder="Story thumbnail url">
                                 <input type="hidden" name="type" value="story">
                              </div>
                            </div>

                            <br>
                            <div class="row">
                              <div class="col-6">
                                <input type="text" class="form-control" name="pdfurl" placeholder="Story pdf link">
                                <br>
                              </div>
                            </div>
                            <hr>
                            <div class="row">

                              <div class="col">
                                <textarea class="form-control" rows="5" name="description" spellcheck="true"
                                  placeholder=" Story description"></textarea>

                              </div>
                            </div>
                            <br>
                            <div class="row">
                             <input type="submit" name="submit" class="btn btn-success">
                            </div>

                          </form>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
  <div class="fixed-plugin">
    <div class="dropdown show-dropdown">
      <a href="#" data-toggle="dropdown">
        <i class="fa fa-cog fa-2x"> </i>
      </a>
      <ul class="dropdown-menu">
        <li class="header-title"> Sidebar Filters</li>
        <li class="adjustments-line">
          <a href="javascript:void(0)" class="switch-trigger active-color">
            <div class="badge-colors ml-auto mr-auto">
              <span class="badge filter badge-purple active" data-color="purple"></span>
              <span class="badge filter badge-azure" data-color="azure"></span>
              <span class="badge filter badge-green" data-color="green"></span>
              <span class="badge filter badge-warning" data-color="orange"></span>
              <span class="badge filter badge-danger" data-color="danger"></span>
            </div>
            <div class="clearfix"></div>
          </a>
        </li>
        <li class="header-title">Images</li>
        <li>
          <a class="img-holder switch-trigger" href="javascript:void(0)">
            <img
              src=""
              alt="">
          </a>
        </li>
        <li class="active">
          <a class="img-holder switch-trigger" href="javascript:void(0)">
            <img
              src=""
              alt="">
          </a>
        </li>
        <li>
          <a class="img-holder switch-trigger" href="javascript:void(0)">
            <img
              src=""
              alt="">
          </a>
        </li>
        <li>
          <a class="img-holder switch-trigger" href="javascript:void(0)">
            <img
              src=""
              alt="">
          </a>
        </li>

      </ul>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="assets/js/core/jquery.min.js"></script>
  <script src="assets/js/core/popper.min.js"></script>
  <script src="assets/js/core/bootstrap-material-design.min.js"></script>
  <script src="https://unpkg.com/default-passive-events"></script>
  <script src="assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!-- Place this tag in your head or just before your close body tag. -->
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chartist JS -->
  <script src="assets/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="assets/js/material-dashboard.js?v=2.1.0"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <script src="assets/demo/demo.js"></script>
  <script>
    $(document).ready(function () {


      // variable
      var i = 1 ;

      // add row to the form

      $("#addimagelink").click(function (e) {
      
      

        $("#imagelinkcont").append(' <hr><div class="col-12">  <input id="imagelinkmain" type="text" class=" form-control"placeholder="image url" name="image[]"> <br> <button type="button" rel="tooltip" title="Add new image" id="remove"class="btn btn-white btn-link btn-sm"><i class="material-icons">remove_circle</i></button> <br>')
        i++;
        console.log(i)
      });

      // Remove row from the form


      $("#imagelinkcont").on('click', '#remove', function (e) {

        $(this).parent('div').remove();
      });

      //  populate values from the first row 










      $().ready(function () {
        $sidebar = $('.sidebar');

        $sidebar_img_container = $sidebar.find('.sidebar-background');

        $full_page = $('.full-page');

        $sidebar_responsive = $('body > .navbar-collapse');

        window_width = $(window).width();

        $('.fixed-plugin a').click(function (event) {
          // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
          if ($(this).hasClass('switch-trigger')) {
            if (event.stopPropagation) {
              event.stopPropagation();
            } else if (window.event) {
              window.event.cancelBubble = true;
            }
          }
        });

        $('.fixed-plugin .active-color span').click(function () {
          $full_page_background = $('.full-page-background');

          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-color', new_color);
          }

          if ($full_page.length != 0) {
            $full_page.attr('filter-color', new_color);
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.attr('data-color', new_color);
          }
        });

        $('.fixed-plugin .background-color .badge').click(function () {
          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('background-color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-background-color', new_color);
          }
        });

        $('.fixed-plugin .img-holder').click(function () {
          $full_page_background = $('.full-page-background');

          $(this).parent('li').siblings().removeClass('active');
          $(this).parent('li').addClass('active');


          var new_image = $(this).find("img").attr('src');

          if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            $sidebar_img_container.fadeOut('fast', function () {
              $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
              $sidebar_img_container.fadeIn('fast');
            });
          }

          if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $full_page_background.fadeOut('fast', function () {
              $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
              $full_page_background.fadeIn('fast');
            });
          }

          if ($('.switch-sidebar-image input:checked').length == 0) {
            var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
          }
        });

        $('.switch-sidebar-image input').change(function () {
          $full_page_background = $('.full-page-background');

          $input = $(this);

          if ($input.is(':checked')) {
            if ($sidebar_img_container.length != 0) {
              $sidebar_img_container.fadeIn('fast');
              $sidebar.attr('data-image', '#');
            }

            if ($full_page_background.length != 0) {
              $full_page_background.fadeIn('fast');
              $full_page.attr('data-image', '#');
            }

            background_image = true;
          } else {
            if ($sidebar_img_container.length != 0) {
              $sidebar.removeAttr('data-image');
              $sidebar_img_container.fadeOut('fast');
            }

            if ($full_page_background.length != 0) {
              $full_page.removeAttr('data-image', '#');
              $full_page_background.fadeOut('fast');
            }

            background_image = false;
          }
        });

        $('.switch-sidebar-mini input').change(function () {
          $body = $('body');

          $input = $(this);

          if (md.misc.sidebar_mini_active == true) {
            $('body').removeClass('sidebar-mini');
            md.misc.sidebar_mini_active = false;

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

          } else {

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

            setTimeout(function () {
              $('body').addClass('sidebar-mini');

              md.misc.sidebar_mini_active = true;
            }, 300);
          }

          // we simulate the window Resize so the charts will get updated in realtime.
          var simulateWindowResize = setInterval(function () {
            window.dispatchEvent(new Event('resize'));
          }, 180);

          // we stop the simulation of Window Resize after the animations are completed
          setTimeout(function () {
            clearInterval(simulateWindowResize);
          }, 1000);

        });
      });
    });
  </script>
  <script>
    $(document).ready(function () {
      // Javascript method's body can be found in assets/js/demos.js
      md.initDashboardPageCharts();

    });
  </script>
</body>

</html>